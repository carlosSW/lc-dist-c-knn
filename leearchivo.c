#include <stdio.h>
#include <stdlib.h>
#define ERROR -1

int leedato(float *dato,FILE *file, int DIM);
int leedato_coma(float *dato,FILE *file, int DIM);
void copiavalor(float *a, float *b, int DIM);
void scanFile(char *ruta, float **datos, int DIM, int numElement);
void scanMetaDatosLC(char *ruta, int *num_centers, int *b_size);
void scanFileLC(char *ruta, float **centers, int num_centers, float *radius, float **clusters, int b_size, int dim);

void scanFile(char *ruta, float **datos, int DIM, int numElement)
{	
	float dato[DIM];
	int i;
	FILE *f_dist;

	printf("\nAbriendo %s... ", ruta);
	
	f_dist = fopen(ruta, "r");
	printf("OK\n");
	fflush(stdout);
	
   printf("\nLeyendo DB... ");
	fflush(stdout);
   for (i=0; i<numElement; i++)
    {
       if (leedato_coma(dato, f_dist, DIM)==ERROR || feof(f_dist))//Si hay un error o se alcanzo el final del archivo
       {
          printf("\n\nERROR :: N_DB mal establecido\n\n");
          fflush(stdout);
          fclose(f_dist);
          break;
       }
       copiavalor(datos[i], dato, DIM);
    }
   fclose(f_dist);//Cerrando el archivo que se abrio con fopen
    printf("OK\n");
	
    fflush(stdout);
}
void scanMetaDatosLC(char *ruta, int *num_centers, int *b_size)
{
   int dato;
   FILE *f_dist;
   printf("\nAbriendo %s...", ruta);
   f_dist = fopen(ruta, "r");
   printf("OK\n");
   fflush(stdout);
   
   printf("\nLeyendo metadatos... ");
	fflush(stdout);

	if(fscanf(f_dist, "%d", &dato)<1)
      printf("Error leyendo num_centers");
   *num_centers=dato;
	if(fscanf(f_dist, "%d", &dato)<1)
      printf("Error leyendo b_size");
   *b_size = dato;
   fclose(f_dist);
}
void scanFileLC(char *ruta, float **centers, int num_centers, float *radius, float **clusters, int b_size, int dim)
{
   FILE *f_dist;
   int i;
   float dato[dim];
   int aux;
   printf("\nAbriendo %s...", ruta);
   f_dist = fopen(ruta, "r");
   printf("OK\n");
   fflush(stdout);
   
   printf("\nLeyendo DB... ");
	fflush(stdout);

	if(fscanf(f_dist, "%d", &aux)<1)
      printf("Error leyendo num_centers");

	if(fscanf(f_dist, "%d", &aux)<1)
      printf("Error leyendo b_size");



   for(i=0; i<num_centers; i++)
   {
      if(leedato(dato, f_dist, dim) == ERROR)
      {
         printf("\nERROR leyendo centers\n");
         fclose(f_dist);
         break;
      }
      copiavalor(centers[i], dato, dim);
   }
   
   if(leedato(radius, f_dist, num_centers) == ERROR)
   {
      printf("\nERROR leyendo radios\n");
   }
   int lim_cl;
	if(fscanf(f_dist, "%d", &lim_cl)<1)
      printf("Error leyendo lim_cl");
   for(i=0; i<b_size*num_centers; i++)
   {
      if(leedato(dato, f_dist, dim)==ERROR)
      {
         printf("\nERROR leyendo clusters\n");
         fclose(f_dist);
         break;
      }
      copiavalor(clusters[i], dato, dim);
   }
   fclose(f_dist);
}

int leedato(float *dato,FILE *file, int DIM)
{
   int i; 
   float num_f;

   for (i=0;i<DIM;i++)
   {
      if (fscanf(file, "%f", &num_f) < 1)
         return ERROR;
      dato[i] = (float)num_f;
   }
   return 1;
}
int leedato_coma(float *dato,FILE *file, int DIM)
{
   int i;
   int num_f;

   for (i=0;i<DIM;i++)
   {
      if (fscanf(file, "%d", &num_f) < 1)
         return ERROR;

      dato[i] = (float)num_f;
      
      if (i+1 < DIM)
         if (fgetc(file) != ',')
         {
            printf("\nERROR :: ',' no encontrada\n");
            return ERROR;
         }
   }
   return 1;
}


void copiavalor(float *a, float *b, int DIM)
{
   int i;
   for (i=0; i<DIM; i++)
      a[i] = b[i];
   return;
}
