struct _Elem
{
  float dist;
  int ind;
};
typedef struct _Elem Elem;
void inserta2(Elem *heap, Elem *elem, int *n_elem);
void extrae2(Elem *heap, int *n_elem, Elem *elem_extraido);
void popush2(Elem *heap, int *n_elem, Elem *elem);
float topH(Elem *heap, int *n_elem);

void inserta2(Elem *heap, Elem *elem, int *n_elem)
{
   int i;
   Elem temp;

   heap[*n_elem].dist = elem->dist;
   heap[*n_elem].ind = elem->ind;
   (*n_elem)++;
   for (i = *n_elem; i>1 && heap[i-1].dist > heap[(i/2)-1].dist; i=i/2)
   {
      //Intercambiamos con el padre
      temp = heap[i-1];
      heap[i-1] = heap[(i/2)-1];
      heap[(i/2)-1] = temp;
   }
}

//inserta2 y extrae2 usan el arreglo del heap desde el elemento 0
void extrae2(Elem *heap, int *n_elem, Elem *elem_extraido)
{
   int i, k;
   Elem temp;
   
   (*elem_extraido).dist = heap[0].dist;
   (*elem_extraido).ind = heap[0].ind;

   heap[0] = heap[(*n_elem)-1];  // Movemos el ultimo a la raiz y achicamos el heap
   (*n_elem)--;
   i = 1;
   while(2*i <= *n_elem) // mientras tenga algun hijo
   {
      k = 2*i; //el hijo izquierdo
      if(k+1 <= *n_elem && heap[(k+1)-1].dist > heap[k-1].dist)
        k = k+1;  //el hijo derecho es el mayor
      if(heap[i-1].dist > heap[k-1].dist)
        break;  //es mayor que ambos hijos

    temp = heap[i-1];  
    heap[i-1] = heap[k-1];
    heap[k-1] = temp;  
    i = k;   //lo intercambiamos con el mayor hijo
   }
    return;
}

void popush2(Elem *heap, int *n_elem, Elem *elem)
{
   int i, k;
   Elem temp;
   
   heap[0].dist = elem->dist;
   heap[0].ind = elem->ind;

   i = 1;
   while(2*i <= *n_elem) // mientras tenga algun hijo
   {
      k = 2*i; //el hijo izquierdo
      if(k+1 <= *n_elem && heap[(k+1)-1].dist > heap[k-1].dist)
        k = k+1;  //el hijo derecho es el mayor
      if(heap[i-1].dist > heap[k-1].dist)
        break;  //es mayor que ambos hijos

      temp = heap[i-1];  
      heap[i-1] = heap[k-1];
      heap[k-1] = temp;  
      i = k;   //lo intercambiamos con el mayor hijo
   }
      return;
}

float topH(Elem *heap, int *n_elem)
{
   if (*n_elem == 0)
      return MAXFLOAT;
   return heap[0].dist;
}
