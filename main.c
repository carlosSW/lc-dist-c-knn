#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <values.h>
#include <math.h>
#include "similitud.c"
#include "leearchivo.c"
//para el calculo de tiempos
#include <sys/resource.h>
#include <time.h>
#include <sys/time.h>

#define DIM 64
#define ERROR -1

#define ALIGN 32

int main(int argc, char *argv[])
{
   /*
    * indice
    * clusters son los clusters del indice
    * centers son los centros de los clusters
    * radius son los radios de los clusters
    * b_zise es la cantidad de elementos de los clusters
    * num_clusters es la cantidad de clusters del indice
    */
   float **clusters, **centers, *radius;
   int b_size, num_centers;
   char *ruta_db;
   /* variables para mapeo de matrices a vectores*/
   float *clusters_vector, *centers_vector;
   
   /*variables para leer consultas*/
	float **queries;
   int num_queries;
   char *ruta_queries;
   /*variables para mapear matrices a vectores*/
   float *queries_vector;
   /*
    * dim es la dimencion de los vectores del indice y de las consultas
    */
   int dim;
   Elem *resp;
   long long resp_total;
	int i,j, l;
   /* k es el numero de k vecinos mas cercanos*/
   int k;
   /*variables para medir el tiempo */
   struct rusage r1, r2;
   float user_time, sys_time, real_time;
   struct timeval t1, t2;
   
	if (argc != 6)
	{
		printf("Error :: Ejecutar como : a.out archivo_Indice archivo_queries Num_queries dim k\n");
		return 1;
	}
   ruta_db = argv[1];
   ruta_queries = argv[2];
   num_queries = atoi(argv[3]);
   dim = atoi(argv[4]);
   k = atoi(argv[5]);
   num_centers=0;
   b_size=0;
   scanMetaDatosLC(ruta_db, &num_centers, &b_size);
//   printf("num_centers = %d\nb_size = %d\n", num_centers, b_size);
   centers = (float **)malloc(sizeof(float *)*num_centers);
   for(i=0; i<num_centers; i++)
      centers[i] = (float *)malloc(sizeof(float)*dim);
   radius = (float *)malloc(sizeof(float)*num_centers);
   clusters = (float **)malloc(sizeof(float *)*num_centers*b_size);
   for(i=0; i<num_centers*b_size; i++)
      clusters[i] = (float *)malloc(sizeof(float)*dim);
   scanFileLC(ruta_db, centers, num_centers, radius, clusters, b_size, dim);
   
   queries = (float **)malloc(sizeof(float *)*num_queries); 
   for(i=0; i<num_queries; i++)
      queries[i] = (float *)malloc(sizeof(float)*dim);
   scanFile(ruta_queries, queries, dim, num_queries);
   clusters_vector = (float *)_mm_malloc(sizeof(float)*num_centers*b_size*dim, ALIGN);
   centers_vector = (float *)_mm_malloc(sizeof(float)*num_centers*dim, ALIGN);
   queries_vector = (float *)_mm_malloc(sizeof(float)*num_queries*dim, ALIGN);

   matrixToVector(clusters, dim, num_centers*b_size, clusters_vector);
   matrixToVector(centers, dim, num_centers, centers_vector);
   matrixToVector(queries, dim, num_queries, queries_vector);
   resp = (Elem *)malloc(sizeof(Elem)*num_queries*num_centers*k);
   //Busqueda
      getrusage(RUSAGE_SELF, &r1);
	  	gettimeofday(&t1, 0);
 
      searchByRange_lc(clusters_vector, centers_vector, radius, num_centers, b_size, queries_vector, num_queries, dim, k, resp);  
	  	gettimeofday(&t2, 0);
  	  	getrusage(RUSAGE_SELF, &r2);
/*   for(i=0; i<num_queries; i++)
   {
      printf("\nq %d\n", i);
      for(j=0; j<k; j++)
      {
         printf("ind = %d d = %f\n", resp[i*num_centers*k+j].ind, resp[i*num_centers*k+j].dist);
      }
   }*/
   //  num_centers = 1;
   //  num_queries = 1;
/*	  for(i=0; i<num_queries; i++)
     {
        printf("q %d\n", i);
        for(j=0; j<num_centers; j++)
        {
           printf("centro %d\n", j);
           for(l=0; l<k; l++)
              printf("i= %d; d = %f\n", resp[k*num_centers*i+k*j+l].ind, resp[k*num_centers*i+k*j+l].dist);
        }
     }  */
 /*   for(i=0; i<num_queries; i++)
    {
      printf("i = %d; d = %f\n", resp[i*num_centers].ind, resp[i*num_centers].dist);
    } */
   /*   resp_total = 0;
      for(i=0; i<num_queries*num_centers; i++)
         resp_total += (long)(long)resp[i];
      printf("total %lld\n", resp_total);*/
      user_time = (r2.ru_utime.tv_sec - r1.ru_utime.tv_sec) + (r2.ru_utime.tv_usec - r1.ru_utime.tv_usec)/1000000.0;
	  	sys_time = (r2.ru_stime.tv_sec - r1.ru_stime.tv_sec) + (r2.ru_stime.tv_usec - r1.ru_stime.tv_usec)/1000000.0;
	  	real_time = (t2.tv_sec - t1.tv_sec) + (float)(t2.tv_usec - t1.tv_usec)/1000000;
      	  	
      printf("\nTiempo CPU = %f", user_time + sys_time);
	  	printf("\nTiempo Real = %f", real_time);
      printf("\n------------\n");
      fflush(stdout);
	return(0);
}

