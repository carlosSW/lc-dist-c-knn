#include <omp.h>
#include <math.h>
/*
 * matrixToVector mapea una matriz de 2 dimenciones a un vector
 * matrix es una matriz de 2 dimensiones
 * num_cols es el numero de colmnas de la matriz
 * num_rows es el numero de filas de la matriz
 * vector es el vector en donde se mapeara la matriz de 2 dimensiones
 */
void matrixToVector(float **matrix, int num_cols, int num_rows, float *vector);
/*
 * distancia retorna la distancia euclidiana entre 2 vectores
 * v1 y v2 son vectores
 * dim es la dimension de los vectores v1 y v2
 */
float distancia(float *v1, float *v2, int dim);
/*
 * imprimir imprime un vector
 * vector es un vector a imprimir
 * dim es la dimension del vector a imprimir
 */
void imprimir(float *vector, int dim);

void matrixToVector(float **matrix, int num_cols, int num_rows, float *vector)
{
   int i,j;
   for(i=0; i<num_rows; i++)
      for(j=0; j<num_cols; j++)
         vector[(i*num_cols)+j] = matrix[i][j];
}
float distancia(float *v1, float *v2, int dim)
{
    int i=0;
    float suma=0.0;
   
   #pragma vector aligned
   #pragma ivdep
   #pragma simd
	for (i=0; i<dim; i++)
   {
      suma += (v1[i]-v2[i])*(v1[i]-v2[i]);
   }
    
	return sqrt(suma);
}
void imprimir(float *vector, int dim)
{
	int i;
	for(i=0;i<dim;i++)
		printf("%lf ", vector[i]);
	printf("\n");
}
