#include "vectores.c"
#include "heap_function.c"

#define TXC 2
#define DELTA 385
#define I_RANGE 200

/*retorna la cantidad de elementos en un rango*/
void searchByRange_exhaustive(float *db, int num_element, float *queries, int num_queries, int dim, float range, long *resp);
void searchByRange_lc(float *clusters, float *centers, float *radius, int num_centers, int b_size, float *queries, int num_queries, int dim, int knn, Elem *resp);
void reductionHeap(Elem *resp, int num_centers, int knn, int num_queries);
void searchKNN(float *db, int num_element, float *queries, int num_queries, int dim, int knn, Elem *resp);
void searchByRange_exhaustive(float *db, int num_element, float *queries, int num_queries, int dim, float range, long *resp)
{
   int i, j;
   
   #pragma omp parallel for private(j)
   for(i=0; i<num_queries; i++)
   {
      resp[i] = 0;
      for(j=0; j<num_element; j++)
         if(distancia(&(queries[i*dim]), &(db[j*dim]), dim)<=range)
            resp[i]++;
   }
}
void searchByRange_lc(float *clusters, float *centers, float *radius, int num_centers, int b_size, float *queries, int num_queries, int dim, int knn, Elem *resp)
{
   int num_threads;
   #pragma omp parallel shared(num_threads)
   {
      int thread_num;
      int n_elem, i, j, k, cont;
      Elem *heap, e_temp;
      float range, d;
      heap = (Elem *)malloc(sizeof(Elem)*knn);
      thread_num = omp_get_thread_num();
      #pragma omp master
      {
         num_threads = omp_get_num_threads();
         printf("Run with %d threads\n", num_threads);
      }
      #pragma omp barrier
      
      for(i=thread_num/TXC; i<num_centers; i+=(num_threads/TXC))
      {
         for(j=thread_num%TXC; j<num_queries; j+=TXC)
         {
            cont=0;
            for(range=I_RANGE; range<MAXFLOAT&&cont<knn; range+=DELTA)
            {
               n_elem = 0;
               d = distancia(&(centers[i*dim]), &(queries[j*dim]), dim);
               if(d<=radius[i]+range)
               {
                  if(d<=range)
                  {
                     cont++;
                     e_temp.dist = d;
                     e_temp.ind = b_size*(i+1)+1;
                     inserta2(&(resp[j*num_centers*knn+i*knn]), &e_temp, &n_elem);
                  }
                  for(k=0; k<b_size; k++)
                  {
                     d = distancia(&(clusters[i*b_size*dim+(k*dim)]), &(queries[j*dim]), dim);
                     if(d<=range)
                     {
                        cont++;
                        if(n_elem<knn)
                        {
                           e_temp.dist = d;
                           e_temp.ind = b_size*i+k;
                           inserta2(&(resp[j*num_centers*knn+i*knn]), &e_temp, &n_elem);
                        }
                        else
                        {
                           if(d<topH(&(resp[j*num_centers*knn+i*knn]), &n_elem))
                           {
                              e_temp.dist = d;
                              e_temp.ind = b_size*i+k;
                              popush2(&(resp[j*num_centers*knn+i*knn]), &n_elem, &e_temp);
                           }
                        }
                     }
                  }
               }
  //             printf("ind = %d cont = %d, range = %f\n", j*num_centers+i, cont, range);
            }
          /*  d = distancia(&(centers[i*dim]), &(queries[j*dim]), dim);
            n_elem = 0;
            if(d<=radius[i]+range)
            {
               if(d<=range)
               {
                  e_temp.dist = d;
                  e_temp.ind = b_size*(i+1)+1;
                  inserta2(heap, &e_temp, &n_elem);
               }
               for(k=0; k<b_size; k++)
               {
                  d = distancia(&(clusters[i*b_size*dim+(k*dim)]), &(queries[j*dim]), dim);
                  if(d<=range)
                  {
                     if(n_elem<knn)
                     {
                        e_temp.dist = d;
                        e_temp.ind = b_size*i+k;
                        inserta2(heap, &e_temp, &n_elem);
                     }
                     else
                     {
                        if(d<topH(heap, &n_elem))
                        {
                           e_temp.dist = d;
                           e_temp.ind = b_size*i+k;
                           popush2(heap, &n_elem, &e_temp);
                        }
                     }
                  }
               }
            }*/
         /*   for(k=0; k<knn; k++)
            {
               extrae2(heap, &n_elem, &e_temp);
               resp[j*num_centers*knn+i*knn+k].ind = e_temp.ind;
               resp[j*num_centers*knn+i*knn+k].dist = e_temp.dist;
            }  */ 
     //       resp[j*num_centers+i].dist=range;
     //       resp[j*num_centers+i].ind=cont;
         }
      }
    //  free(heap);
   }//fin parallel
   printf("End\n");
   
   reductionHeap(resp, num_centers, knn, num_queries);
}
void reductionHeap(Elem *resp, int num_centers, int knn, int num_queries)
{
   int num_threads;
   #pragma omp parallel
   {
      int i, j, k, thread_num, n_elem;
      Elem *heap, e_temp;
      heap = (Elem *)malloc(sizeof(Elem)*knn);
      thread_num = omp_get_thread_num();
      #pragma omp master
      {
         num_threads = omp_get_num_threads();
         printf("Run with %d threads\n", num_threads);
      }
      #pragma omp barrier
      for(i=thread_num; i<num_queries; i+=num_threads)
      {
         n_elem = 0;
         for(j=0; j<knn; j++)
         {
            e_temp.dist = resp[i*num_centers*knn+j].dist;
            e_temp.ind = resp[i*num_centers*knn+j].ind;
            inserta2(heap, &e_temp, &n_elem);
         }
         for(j=1; j<num_centers; j++)
         {
            for(k=knn-1; k>=0; k--)
            {
               if(resp[i*num_centers*knn+j*knn+k].dist<topH(heap, &n_elem))
               {
                  e_temp.dist = resp[i*num_centers*knn+j*knn+k].dist;
                  e_temp.ind = resp[i*num_centers*knn+j*knn+k].ind;
                  popush2(heap, &n_elem, &e_temp);
               }
               else
               {
                  break;
               }
            }
         }
         for(j=0; j<knn; j++)
         {
            extrae2(heap, &n_elem, &e_temp);
            resp[i*num_centers*knn+j].dist = e_temp.dist;
            resp[i*num_centers*knn+j].ind = e_temp.ind;
         }
      }
      free(heap);
   }
}
void searchKNN(float *db_vector, int num_element, float *queries_vector, int num_queries, int dim, int knn, Elem *resp)
{
   int num_threads;
   #pragma omp parallel shared(num_threads)
   {
      int thread_num, n_elem, i, j, k;
      Elem *heap, e_temp;
      float d;
      heap = (Elem *)malloc(sizeof(Elem)*knn);
      thread_num = omp_get_thread_num();
      #pragma omp master
      {
         num_threads = omp_get_num_threads();
         printf("Run with %d threads\n", num_threads);
      }
      #pragma omp barrier
      for(i=thread_num; i<num_queries; i+=num_threads)
      {
         n_elem = 0;
         for(j=0; j<knn; j++)
         {
            e_temp.dist = distancia(&queries_vector[i*dim], &db_vector[j*dim], dim);
            e_temp.ind = j;
            inserta2(heap, &e_temp, &n_elem);
         }
         for(j=knn; j<num_element; j++)
         {
            d = distancia(&queries_vector[i*dim], &db_vector[j*dim], dim);
            if(d<topH(heap, &n_elem))
            {
               e_temp.dist = d;
               e_temp.ind = j;
               popush2(heap, &n_elem, &e_temp);
            }
         }
         for(j=0; j<knn; j++)
         {
            extrae2(heap, &n_elem, &e_temp);
            resp[i*knn+j].dist = e_temp.dist;
            resp[i*knn+j].ind = e_temp.ind;
         }
      }
      free(heap);
   }
}
